extends Control

var exit_timer
var sound_start
var sound_select

var sound_played = false


func _ready():
	exit_timer = 0
	sound_start = load("res://Start.ogg")
	sound_select = load("res://Select.ogg")
	
	$AudioStreamPlayer.stream = sound_start
	$AudioStreamPlayer.play()


func _process(delta):
	$LabelFPS.text = "FPS: " + str(Engine.get_frames_per_second()) + "\n"
	$LabelFPS.text += "Delta Time: " + str(delta) + "\n"
	if OS.vsync_enabled:
		$LabelFPS.text += "Vsync Enabled"
	else:
		$LabelFPS.text += "Vsync Disabled"
		
	if Input.is_action_pressed("menu"):
		exit_timer += delta
		$ExitProgressBar.rect_size.x += delta * 280
		if $ExitProgressBar.rect_size.x >= 280:
			$ExitProgressBar.rect_size.x = 280
			get_tree().quit()
		
	if Input.is_action_just_released("menu"):
		if exit_timer > 0 and exit_timer < 1:
			OS.vsync_enabled = not OS.vsync_enabled
			$ExitProgressBar.rect_size.x = 0
			exit_timer = 0


func _input(event):
	if event is InputEventKey:
		$LabelKeyPress.text = OS.get_scancode_string(event.get_scancode_with_modifiers()) + " Key Pressed"
		if not sound_played:
			$AudioStreamPlayer.stream = sound_select
			$AudioStreamPlayer.play()
			sound_played = true
			
		if not event.pressed:
			sound_played = false
